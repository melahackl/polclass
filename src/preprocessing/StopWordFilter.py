'''
Created on 12.09.2017

@author: jeric
'''
#TODO: optimize
from builtins import set
import os
filter_set = set()

def filter_words(text=""):
    array = text.split(" ")
    output = ""
    for var in array:
        if var not in filter_set:
            output = output + " "+ var

    return output
    
def build_filter(array=[[]]):
    for var in array:
        filter_set.add(var)
        
def default_filter():
    filepth = os.path.join(os.path.dirname(__file__),'..','..', 'resources/stopwords.csv')
    print(filepth) 
    file = open(filepth, "r",  encoding="utf-8")
    csv = file.read()
    build_filter(csv.split(","))
    