'''
Created on 19.09.2017

@author: jeric
'''
from bson import Binary, Code
from bson.json_util import dumps, CANONICAL_JSON_OPTIONS
text = dumps([{'foo': [1, 2]},{'bar': {'hello': 'world'}},{'code': Code("function x() { return 1; }")},{'bin': Binary(b"")}],json_options=CANONICAL_JSON_OPTIONS)
print(text)