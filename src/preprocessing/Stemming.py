from nltk import PorterStemmer
'''
Created on 05.12.2017

@author: jeric
'''
def do_stemming(filtered):
    stemmed = []
    for f in filtered:
        stemmed.append(PorterStemmer().stem(f))
        #stemmed.append(LancasterStemmer().stem(f))
        #stemmed.append(SnowballStemmer('english').stem(f))
    return stemmed