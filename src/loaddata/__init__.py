import keras.preprocessing.text
from preprocessing.StopWordFilter import filter_words, build_filter, default_filter
from keras.layers.embeddings import Embedding

import json
import preprocessing
from preprocessing import ConvertText, StopWordFilter, Stemming
from numpy import array
from numpy import empty
from numpy import asarray
import numpy as np
from sklearn.preprocessing import LabelBinarizer
from setuptools.dist import sequence
import bson
import os
from keras.preprocessing import sequence


def getLabelCount():
    filepth = os.path.join(os.path.dirname(__file__),'..','..', 'resources/cleaned_data.bson')
    print(filepth)
    bson_file = open(filepth, 'rb')
    b = bson.decode_all(bson_file.read())
    parsed= b
    seed=7
    np.random.seed(seed)
    np.random.shuffle(parsed) 
    #     print(json.dumps(text, indent=4, sort_keys=True))
    #     hashset.add(json.dumps(text, indent=4, sort_keys=True))
    
    
    hashset = set();
    for x in range(0, len(parsed)):
        label = parsed[x]["classification_level1"]
        hashset.add(label)
        
    len_labels = len(hashset)
    return len_labels


def get_longest_text_size():
    filepth = os.path.join(os.path.dirname(__file__),'..','..', 'resources/cleaned_data.bson')
    print(filepth)
    bson_file = open(filepth, 'rb')
    b = bson.decode_all(bson_file.read())
    parsed= b
    max_len=0
    for var in b:
        text = var["text"]
        vector = ConvertText.convert(json.dumps(text, indent=4, sort_keys=True))
        temp_len = len(vector)
        if temp_len > max_len:
            max_len=temp_len
    return max_len

def loaddata():
    text = StopWordFilter.default_filter()

    filepth = os.path.join(os.path.dirname(__file__),'..','..', 'resources/cleaned_data.bson')
    print(filepth)
    bson_file = open(filepth, 'rb')
    b = bson.decode_all(bson_file.read())
    parsed= b

    seed=7
    np.random.seed(seed)
    np.random.shuffle(parsed)
    
    hashset = set();
    for x in range(0, len(parsed)):
        label = parsed[x]["classification_level1"]
        hashset.add(label)
    
        
    
    encoder = LabelBinarizer()
    transformed_label = encoder.fit_transform(list(hashset))
    
    print(transformed_label)
    temp_x_data = list()
    y_data_temp = empty([len(parsed)], dtype="<U20")
    for x in range(0, len(parsed)):
        label = parsed[x]["classification_level1"]
        text = parsed[x]["text"]
        text = StopWordFilter.filter_words(text)
        text = Stemming.do_stemming(text)
        vector = ConvertText.convert(json.dumps(text, indent=4, sort_keys=True))
        temp_x_data.append(vector)
        y_data_temp[x] = label
    
    y_data = empty([len(parsed)])
    y_data = encoder.transform(y_data_temp)

    x_data = empty([len(parsed), get_longest_text_size()])
    
    sequence.pad_sequences(temp_x_data)
    x_data = sequence.pad_sequences(temp_x_data, maxlen=1000)

    
    factor = 0.7

    splitNumber = int(len(parsed)*factor)
    fullLength = len(parsed)
    
    
    # X_train = empty([splitNumber, 50])
    # Y_train = empty([splitNumber])
    # X_test = empty([fullLength-splitNumber, 50])
    # Y_test = empty([fullLength-splitNumber])
    
    X_train = x_data[0:splitNumber]
    Y_train = y_data[0:splitNumber]
    
    X_test = x_data[splitNumber:fullLength]
    Y_test = y_data[splitNumber:fullLength]
    
    # for x in range(0, splitNumber):
    #     X_train[x] = x_data[x]
    #     Y_train[x] = y_data[x]
    # 
    # for x in range(splitNumber, fullLength):
    #     X_test[x-splitNumber] = x_data[x]
    #     Y_test[x-splitNumber] = y_data[x]
        
        
    return (X_train,Y_train), (X_test,Y_test)
    
