# LSTM for sequence classification in the IMDB dataset
import numpy
#from keras.datasets import imdb
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import Flatten
from keras.layers import LSTM
from keras.layers import GRU
from keras.layers.embeddings import Embedding
from keras.preprocessing import sequence
import loaddata

# fix random seed for reproducibility
numpy.random.seed(7)

# loaddata the dataset but only keep the top n words, zero the rest
top_words = 500
#(X_train, Y_train), (X_test, Y_test) = imdb.load_data(nb_words=top_words)

(X_train, Y_train), (X_test, Y_test) =loaddata.loaddata(); 

# truncate and pad input sequences
max_review_length = 1000
# X_train = sequence.pad_sequences(X_train, maxlen=max_review_length)
# X_test = sequence.pad_sequences(X_test, maxlen=max_review_length)

# create the model
embedding_vecor_length = 32
model = Sequential()
#Input-Layer
model.add(Embedding(top_words, embedding_vecor_length, input_length=max_review_length))
#Hidden-Layer
model.add(GRU(100, dropout=0.2, recurrent_dropout=0.1))
#Output-Layer
model.add(Dense(loaddata.getLabelCount(), activation='sigmoid'))
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
print(model.summary())
model.fit(X_train, Y_train, nb_epoch=2, batch_size=200)

# Final evaluation of the model
scores = model.evaluate(X_test, Y_test, verbose=0)
print("Accuracy: %.2f%%" % (scores[1]*100))